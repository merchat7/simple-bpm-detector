from pydub import AudioSegment
import numpy as np

"""Get average energy in range(n*interval)"""
def get_average_buffer(sound, start, end, interval, n):
    ampt_list = []
    for _ in range(n):
        ampt_list.append(sound[start:end].dBFS)
        start, end = start + interval, end + interval
    return ampt_list

"""
Get average energy in range(n*interval)

Optimize, by reusing previous calculation
- Remove oldest energy (in front of list)
- Add new energy (at end of list)
"""
def get_average_buffer_fast(new_ampt, hist_list):
    new_hist_list = hist_list[1:]
    new_hist_list.append(new_ampt)
    return new_hist_list

"""
Get average time (ms) between beats

A sound interval is considered a beat if 
c * avg(energy(prev_second)) < energy(current_interval) 

There are 3 type of average beats returned
1. Average of every beat intervals
2. Average of every beat intervals with bin_count > threshold & bin_size = interval
3. Mode of every beat intervals with bin_size = interval
* 2 & 3, interval / 2, has been added to get the middle of the bin (as bin_size is a range, not a discrete value)
"""
def get_average_beat_interval(sound, start, end, interval, ampt_list, n):
    isBeat = False
    prev_beat = -1
    beat_intervals = []
    while end < len(sound):
        ampt = sound[start:end].dBFS
        ampt_list = get_average_buffer_fast(ampt, ampt_list)
        average = sum(ampt_list) / n
        variance = sum((ampt - average) ** 2 for ampt in ampt_list) / len(ampt_list)
        c = -0.001 * variance + 1.5142857
        if (1/c) * average < ampt:
            if isBeat != True:
                if prev_beat != -1:
                    beat_interval = start - prev_beat
                    beat_intervals.append(fit_in_range(beat_interval))
                prev_beat = start
            isBeat = True
        else:
            isBeat = False
        start, end = start + interval, end + interval
    average = sum(beat_intervals) / len(beat_intervals)

    bins = np.arange(min(beat_intervals) - interval, max(beat_intervals) + interval, interval)
    hist, bin_edges = np.histogram(beat_intervals, bins=bins)
    new_hist = sorted(list(zip(bin_edges, hist)), key=lambda x: x[1], reverse=True)
    threshold = 0.2
    new_hist = list(filter(lambda x: x[1] > new_hist[0][1] * threshold, new_hist))
    bpm_hist = list(map(lambda x: (bpm(x[0]), x[1]), new_hist))
    print(f"Filtered BPM with count > {threshold} * max_count (BPM, Count): {bpm_hist}")
    mode = new_hist[0][0] + (interval / 2)
    return average, average_tuples(new_hist), mode

"""Get the average of num such that (num, count) -> sum(all(num * count)) / total_count"""
def average_tuples(tups):
    total = sum((num * count for num, count in tups))
    total_count = sum((count for _, count in tups))
    return (total / total_count) + (interval / 2)

"""The bpm using the average interval (ms) between beats"""
def bpm(average_beat_interval):
    return round(60000 / average_beat_interval)

""" 
Return a value that is in [begin, end)

By default, begin = 300, end = 600, as bpm(600) & bpm(300) give the range [100-200)
"""
def fit_in_range(value, begin=300, end=600):
    if value >= end:
        while value >= end:
            value /= 2
    if value < begin:
        while value < begin:
            value *= 2
    return value

songs = [(79, "simple"),
         (119, "electro"),
         (120, "simple"),
         (138, "pop"),
         (140, "pop"),
         (146, "trance"),
         (168, "emotional"),
         (170, "future bass"),
         (180, "rock"),
         (201, "metal"),
         (239, "simple")]

path_to_song = "songs" #in the same directory as this file
for song in songs:
    try:
        name, type = song
        print(f"[BPM for {fit_in_range(name, 100, 200)} ({type})")
    except: # If you do not want to specify type, or use a name that is not an integer
        name = song
        print(f"[BPM for {name}")
    sound = AudioSegment.from_wav(f"{path_to_song}//{name}.wav")

    interval = 25 # 25 & bpm(mode) works surprisingly well
    start = 1000
    end = start + interval

    n = int(1000.0 / interval)
    ampt_list = get_average_buffer(sound, 0, 0 + interval, interval, n)

    average_beat_interval, filtered_beat_interval, mode_beat_interval = get_average_beat_interval(sound, start, end, interval, ampt_list, n)

    print("BPM based on average: ", end='')
    print(f"{bpm(average_beat_interval + interval)}-{bpm(average_beat_interval - interval)} ({bpm(average_beat_interval)})")
    print("BPM based on average (filtered): ", end=''),
    print(f"{bpm(filtered_beat_interval + interval)}-{bpm(filtered_beat_interval - interval)} ({bpm(filtered_beat_interval)})")
    print("BPM based on mode: ", end=''),
    print(f"{bpm(mode_beat_interval + interval)}-{bpm(mode_beat_interval - interval)} ({bpm(mode_beat_interval)})")